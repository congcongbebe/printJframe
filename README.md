# Java一行代码：打印预览+打印
Java 一行代码，实现打印预览+打印功能，不依赖任何包，拿来即用。

## 使用
#### 1. 下载 PrintJframe.java 丢到你的项目中去
#### 2. 一句话使用
````
new PrintJframe().printPreview("/Users/apple/wuyeclient/cache/image/");</pre>
````

传入要打印的文件所在的文件夹，会自动便利这个文件夹下所有的文件进行打印。
比如，在 /Users/apple/wuyeclient/cache/image/ 文件夹下有两个图片 1.png、2.png，那么传入这个文件夹路径后会自动便利出这个文件夹下的这两个png文件，每个图片文件打印预览时都是一页。
注意，只能打印图片png格式文件

支持JDK：8

## 运行效果
![输入图片说明](https://images.gitee.com/uploads/images/2019/0929/162353_99118e7a_429922.png "1.png")